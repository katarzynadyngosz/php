insert into 15_dyngosz.Rola values ('1', 'ROLE_ADMIN');
insert into 15_dyngosz.Rola values ('2', 'ROLE_PRACOWNIK');
insert into 15_dyngosz.Rola values ('3', 'ROLE_FIRMA');

insert into 15_dyngosz.Firma values ('1', 'Firma', 'firma@firma.firma', '16546574', 'Warszawa');
insert into 15_dyngosz.Firma values ('2', 'Firma2', 'firma2@firma.firma', '8645651', 'Helsinki');

insert into 15_dyngosz.Parking values ('1', 'Bonarka', '100', '2.00', '1');

insert into 15_dyngosz.Pracownik(ID_pracownik, login, haslo, FK_rola) values ('1', 'kacia', '$2y$13$t/sI0PDHs/HNVZIXvgbvOuF5cuG3kD7kmiOoX9s4E/f/vuqQWtX0a', '1');
insert into 15_dyngosz.Pracownik(ID_pracownik, login, haslo, FK_firma, FK_rola) values ('2', 'kacia2', '$2y$13$t/sI0PDHs/HNVZIXvgbvOuF5cuG3kD7kmiOoX9s4E/f/vuqQWtX0a', '2', '3');

insert into 15_dyngosz.Dane_pracownika values ('1', 'kacia', 'kacia', 'kacia@kacia.kacia', '1');
insert into 15_dyngosz.Dane_pracownika values ('2', 'kacia2', 'kacia2', 'kacia2@kacia.kacia', '2');

