<?php
/**
 * AdminPracownik type.
 */
namespace Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints as Assert;

use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class AdminWjazdType
 */
class AdminWjazdType extends AbstractType
{
    /**
     * Build form
     *
     * @param \Symfony\Component\Form\FormBuilderInterface $builder
     * @param array                                        $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'nr_biletu',
            TextType::class,
            [
                'data' => isset($options['data']['dane']['nr_biletu']) ? $options['data']['dane']['nr_biletu'] : null,
                'label' => 'adminwjazdlabel.nr_biletu',
                'required' => true,
                'attr' => [
                    'max_length' => 40,
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length(
                        [
                            'max' => 40,
                        ]
                    ),
                    new Assert\Type(
                        [
                            'type' => 'string',
                        ]
                    ),
                ],
            ]
        );
        if (isset($options['data']['dane']['parking'])) {
            $builder->add(
                'parking',
                ChoiceType::class,
                [
                    'data' => isset($options['data']['dane']['parking_nazwa']) ? $options['data']['dane']['parking_nazwa'] : null,
                    'choices' => $options['data']['dane']['parking'],
                    'label' => 'adminwjazdlabel.parking',
                    'required' => true,
                    'attr' => [
                        'max_length' => 128,
                    ],
                    'constraints' => [
                        new Assert\NotBlank(),
                        new Assert\Length(
                            [
                                'max' => 40,
                            ]
                        ),
                    ],
                ]
            );
        }
        $builder->add(
            'koszt',
            NumberType::class,
            [
                'data' => isset($options['data']['dane']['koszt']) ? $options['data']['dane']['koszt'] : null,
                'label' => 'adminwjazdlabel.koszt',
                'required' => true,
                'attr' => [
                    'max_length' => 128,
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length(
                        [
                            'max' => 40,
                        ]
                    ),
                    new Assert\Range(
                        [
                            'min' => 0.00,
                            'max' => 100000000.00,
                        ]
                    ),
                    new Assert\Type(
                        [
                            'type' => 'double',
                        ]
                    ),
                ],
            ]
        );
        $builder->add(
            'czas_wjazdu',
            DateTimeType::class,
            [
                'data' => isset($options['data']['dane']['czas_wjazdu']) ? new \DateTime($options['data']['dane']['czas_wjazdu']) : null,
                'label' => 'adminwjazdlabel.czas_wjazdu',
                'required' => true,
                'attr' => [
                    'max_length' => 128,
                ],
            ]
        );
        $builder->add(
            'czy_czas_wyjazdu',
            CheckboxType::class,
            [
                'required' => false,
                'label' => 'adminwjazdlabel.czy_wyjechal',
            ]
        );
        $builder->add(
            'czas_wyjazdu',
            DateTimeType::class,
            [
                'data' => isset($options['data']['dane']['czas_wyjazdu']) ? new \DateTime($options['data']['dane']['czas_wyjazdu']) : null,
                'label' => 'adminwjazdlabel.czas_wyjazdu',
                'required' => true,
                'attr' => [
                    'max_length' => 128,
                ],
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'adminfirma_type';
    }
}
