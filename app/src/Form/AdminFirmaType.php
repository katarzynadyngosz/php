<?php
/**
 * AdminPracownik type.
 */
namespace Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;
use Validator\Constraints as CustomAssert;

/**
 * Class AdminFirmaType
 */
class AdminFirmaType extends AbstractType
{
    /**
     * Build form
     *
     * @param \Symfony\Component\Form\FormBuilderInterface $builder
     * @param array                                        $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'nazwa',
            TextType::class,
            [
                'data' => isset($options['data']['dane']['nazwa']) ? $options['data']['dane']['nazwa'] : null,
                'label' => 'adminfirmalabel.nazwa',
                'required' => true,
                'attr' => [
                    'max_length' => 20,
                ],
                'constraints' => [
                    new CustomAssert\UniqueNazwa(
                        [
                            'repository' => $options['firma_repository'],
                            'elementId' => isset($options['data']['dane']['id']) ? $options['data']['dane']['id'] : null,
                        ]
                    ),
                    new Assert\NotBlank(),
                    new Assert\Length(
                        [
                            'max' => 20,
                        ]
                    ),
                    new Assert\Type(
                        [
                            'type' => 'string',
                        ]
                    ),
                ],
            ]
        );
        $builder->add(
            'email',
            EmailType::class,
            [
                'data' => isset($options['data']['dane']['email']) ? $options['data']['dane']['email'] : null,
                'label' => 'adminfirmalabel.email',
                'required' => true,
                'attr' => [
                    'max_length' => 40,
                ],
                'constraints' => [
                    new CustomAssert\UniqueEmail(
                        [
                            'repository' => $options['firma_repository'],
                            'elementId' => isset($options['data']['dane']['id']) ? $options['data']['dane']['id'] : null,
                        ]
                    ),
                    new Assert\NotBlank(),
                    new Assert\Email(
                        [
                            'checkMX' => true,
                        ]
                    ),
                    new Assert\Length(
                        [
                            'max' => 40,
                        ]
                    ),

                ],
            ]
        );
        $builder->add(
            'nip',
            TextType::class,
            [
                'data' => isset($options['data']['dane']['nip']) ? $options['data']['dane']['nip'] : null,
                'label' => 'adminfirmalabel.nip',
                'required' => true,
                'attr' => [
                    'max_length' => 40,
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length(
                        [
                            'max' => 40,
                        ]
                    ),
                ],
            ]
        );
        $builder->add(
            'miasto',
            TextType::class,
            [
                'data' => isset($options['data']['dane']['miasto']) ? $options['data']['dane']['miasto'] : null,
                'label' => 'adminfirmalabel.miasto',
                'required' => true,
                'attr' => [
                    'max_length' => 40,
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length(
                        [
                            'max' => 40,
                        ]
                    ),
                    new Assert\Type(
                        [
                            'type' => 'string',
                        ]
                    ),
                ],
            ]
        );
    }

    /**
     * Configure options
     *
     * @param \Symfony\Component\OptionsResolver\OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'firma_repository' => null,
            ]
        );
    }
    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'adminfirma_type';
    }
}
