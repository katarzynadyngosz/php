<?php
/**
 * AdminPracownik type.
 */
namespace Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;
use Validator\Constraints as CustomAssert;

/**
 * Class AdminPracownikType.
 */
class AdminPracownikType extends AbstractType
{
    /**
     * Build form
     *
     * @param \Symfony\Component\Form\FormBuilderInterface $builder
     * @param array                                        $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'login',
            TextType::class,
            [
                'data' => isset($options['data']['dane']['login']) ? $options['data']['dane']['login'] : null,
                'label' => 'adminpracowniklabel.login',
                'required' => true,
                'attr' => [
                    'max_length' => 40,
                ],
                'constraints' => [
                    new CustomAssert\UniqueLogin(
                        [
                            'repository' => $options['pracownik_repository'],
                            'elementId' => isset($options['data']['dane']['id']) ? $options['data']['dane']['id'] : null,
                        ]
                    ),
                    new Assert\NotBlank(),
                    new Assert\Type(
                        [
                            'type' => 'string',
                        ]
                    ),
                    new Assert\Length(
                        [
                            'max' => 40,
                        ]
                    ),
                ],
            ]
        );
        $builder->add(
            'haslo',
            PasswordType::class,
            [
                'label' => 'adminpracowniklabel.haslo',
                'required' => !isset($options['data']['dane']['login']),
                'attr' => [
                    'max_length' => 40,
                ],
                'constraints' => [
                    new Assert\Length(
                        [
                            'max' => 40,
                        ]
                    ),
                ],
            ]
        );
        $builder->add(
            'rola',
            ChoiceType::class,
            [
                'data' => isset($options['data']['dane']['rola_wartosc']) ? $options['data']['dane']['rola_wartosc'] : null,
                'choices' => isset($options['data']['dane']['rola']) ? $options['data']['dane']['rola'] : null,
                'label' => 'adminpracowniklabel.rola',
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length(
                        [
                            'max' => 40,
                        ]
                    ),
                ],
            ]
        );
        $builder->add(
            'imie',
            TextType::class,
            [
                'data' => isset($options['data']['dane']['imie']) ? $options['data']['dane']['imie'] : null,
                'label' => 'adminpracowniklabel.imie',
                'required' => true,
                'attr' => [
                    'max_length' => 40,
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length(
                        [
                            'max' => 40,
                        ]
                    ),

                ],
            ]
        );
        $builder->add(
            'nazwisko',
            TextType::class,
            [
                'data' => isset($options['data']['dane']['nazwisko']) ? $options['data']['dane']['nazwisko'] : null,
                'label' => 'adminpracowniklabel.nazwisko',
                'required' => true,
                'attr' => [
                    'max_length' => 40,
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length(
                        [
                            'max' => 40,
                        ]
                    ),
                ],
            ]
        );
        $builder->add(
            'email',
            EmailType::class,
            [
                'data' => isset($options['data']['dane']['email']) ? $options['data']['dane']['email'] : null,
                'label' => 'adminpracowniklabel.email',
                'required' => true,
                'attr' => [
                    'max_length' => 40,
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length(
                        [
                            'max' => 40,
                        ]
                    ),
                    new Assert\Email(
                        [
                            'checkMX' => true,
                        ]
                    ),
                ],
            ]
        );
        if (isset($options['data']['dane']['firma'])) {
            $builder->add(
                'firma',
                ChoiceType::class,
                [
                    'data' => isset($options['data']['dane']['firma_wartosc']) ? $options['data']['dane']['firma_wartosc'] : null,
                    'choices' => isset($options['data']['dane']['firma']) ? $options['data']['dane']['firma'] : null,
                    'label' => 'adminpracowniklabel.firma',
                    'required' => true,
                    'constraints' => [
                        new Assert\NotBlank(),
                        new Assert\Length(
                            [
                                'max' => 40,
                            ]
                        ),
                    ],
                ]
            );
        }
        $builder->add(
            'parking',
            ChoiceType::class,
            [
                'data' => isset($options['data']['dane']['parking_wartosc']) ? $options['data']['dane']['parking_wartosc'] : null,
                'choices' => isset($options['data']['dane']['parking']) ? $options['data']['dane']['parking'] : null,
                'label' => 'adminpracowniklabel.parking',
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length(
                        [
                            'max' => 40,
                        ]
                    ),
                ],
            ]
        );
    }

    /**
     * Configure options
     *
     * @param \Symfony\Component\OptionsResolver\OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'pracownik_repository' => null,
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'adminpracownik_type';
    }
}
