<?php
/**
 * Klient type.
 */
namespace Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class WyjazdType.
 */
class WyjazdType extends AbstractType
{
    /**
     * Build form
     *
     * @param \Symfony\Component\Form\FormBuilderInterface $builder
     * @param array                                        $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'nr_biletu',
            TextType::class,
            [
                'label' => 'wyjazdlabel.nr_biletu',
                'required' => true,
                'attr' => [
                    'max_length' => 40,
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length(
                        [
                            'max' => 40,
                        ]
                    ),
                    new Assert\Type(
                        [
                            'type' => 'string',
                        ]
                    ),
                ],
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'wyjazd_type';
    }
}
