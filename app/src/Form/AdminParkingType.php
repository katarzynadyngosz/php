<?php
/**
 * AdminPracownik type.
 */
namespace Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;
use Validator\Constraints as CustomAssert;

/**
 * Class AdminParkingType
 */
class AdminParkingType extends AbstractType
{
    /**
     * Build form
     *
     * @param \Symfony\Component\Form\FormBuilderInterface $builder
     * @param array                                        $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'nazwa',
            TextType::class,
            [
                'data' => isset($options['data']['dane']['parking_nazwa']) ? $options['data']['dane']['parking_nazwa'] : null,
                'label' => 'adminparkinglabel.nazwa',
                'required' => true,
                'attr' => [
                    'max_length' => 40,
                ],
                'constraints' => [
                    new CustomAssert\UniqueNazwa(
                        [
                            'repository' => $options['parking_repository'],
                            'elementId' => isset($options['data']['dane']['id']) ? $options['data']['dane']['id'] : null,
                        ]
                    ),
                    new Assert\NotBlank(),
                    new Assert\Length(
                        [
                            'max' => 40,
                        ]
                    ),
                    new Assert\Type(
                        [
                            'type' => 'string',
                        ]
                    ),
                ],
            ]
        );
        $builder->add(
            'liczba_miejsc',
            NumberType::class,
            [
                'data' => isset($options['data']['dane']['liczba_miejsc']) ? $options['data']['dane']['liczba_miejsc'] : null,
                'label' => 'adminparkinglabel.liczba_miejsc',
                'required' => true,
                'attr' => [
                    'max_length' => 40,
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length(
                        [
                            'max' => 40,
                        ]
                    ),
                    new Assert\Range(
                        [
                            'min' => 1,
                            'max' => 100000000,
                        ]
                    ),
                ],
            ]
        );
        $builder->add(
            'kwota_godzina',
            NumberType::class,
            [
                'data' => isset($options['data']['dane']['kwota_godzina']) ? $options['data']['dane']['kwota_godzina'] : null,
                'label' => 'adminparkinglabel.cena',
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length(
                        [
                        'max' => 40,
                        ]
                    ),
                    new Assert\Range(
                        [
                            'min' => 0.00,
                            'max' => 999999.99,
                        ]
                    ),
                    new Assert\Type(
                        [
                            'type' => 'double',
                        ]
                    ),
                ],
            ]
        );
        if (isset($options['data']['dane']['firma'])) {
            $builder->add(
                'firma',
                ChoiceType::class,
                [
                    'data' => isset($options['data']['dane']['firma_nazwa']) ? $options['data']['dane']['firma_nazwa'] : null,
                    'choices' => isset($options['data']['dane']['firma']) ? $options['data']['dane']['firma'] : null,
                    'label' => 'adminpracowniklabel.firma',
                    'required' => true,
                    'constraints' => [
                        new Assert\NotBlank(),
                    ],
                ]
            );
        }
    }

    /**
     * Configure options
     *
     * @param \Symfony\Component\OptionsResolver\OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'parking_repository' => null,
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'adminparking_type';
    }
}
