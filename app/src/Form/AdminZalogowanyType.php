<?php
/**
 * AdminZalogowany type
 */
namespace Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;
use Validator\Constraints as CustomAssert;

/**
 * Class AdminZalogowanyType
 */
class AdminZalogowanyType extends AbstractType
{
    /**
     * Build form
     *
     * @param \Symfony\Component\Form\FormBuilderInterface $builder
     * @param array                                        $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'login',
            TextType::class,
            [
                'data' => isset($options['data']['dane']['login']) ? $options['data']['dane']['login'] : null,
                'label' => 'adminzalogowanylabel.login',
                'required' => true,
                'attr' => [
                    'max_length' => 128,
                ],
                'constraints' => [
                    new CustomAssert\UniqueLogin(
                        [
                            'repository' => $options['pracownik_repository'],
                            'elementId' => isset($options['data']['dane']['id']) ? $options['data']['dane']['id'] : null,
                        ]
                    ),
                    new Assert\NotBlank(),
                    new Assert\Length(
                        [
                            'max' => 40,
                        ]
                    ),
                    new Assert\Type(
                        [
                            'type' => 'string',
                        ]
                    ),
                ],
            ]
        );
        $builder->add(
            'haslo',
            PasswordType::class,
            [
                'label' => 'adminzalogowanylabel.haslo',
                'required' => !isset($options['data']['dane']['login']),
                'attr' => [
                    'max_length' => 40,
                ],
                'constraints' => [
                    new Assert\Length(
                        [
                            'max' => 40,
                        ]
                    ),
                ],
            ]
        );
        $builder->add(
            'email',
            EmailType::class,
            [
                'data' => isset($options['data']['dane']['email']) ? $options['data']['dane']['email'] : null,
                'label' => 'adminzalogowanylabel.email',
                'required' => true,
                'attr' => [
                    'max_length' => 128,
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Email(
                        [
                            'message' => 'walidacja.email',
                            'checkMX' => true,
                        ]
                    ),
                    new Assert\Length(
                        [
                            'max' => 40,
                        ]
                    ),

                ],
            ]
        );
    }

    /**
     * Configure options
     *
     * @param \Symfony\Component\OptionsResolver\OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'pracownik_repository' => null,
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'adminzalogowany_type';
    }
}
