<?php
/**
 * AdminWjazd controller
 */
namespace Controller;

use Repository\ParkingRepository;
use Repository\PracownikRepository;
use Repository\WjazdRepository;
use Silex\Application;
use Silex\Api\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Form\AdminWjazdType;

/**
 * Class AdminWjazdController
 */
class AdminWjazdController implements ControllerProviderInterface
{
    /**
     * Routing settings.
     *
     * @param \Silex\Application $app Silex application
     *
     * @return \Silex\ControllerCollection Result
     */
    public function connect(Application $app)
    {
        $controller = $app['controllers_factory'];
        $controller->get('/', [$this, 'indexAction'])
            ->bind('adminwjazd');
        $controller->get('/page/{page}', [$this, 'indexAction'])
            ->value('page', 1)
            ->bind('adminwjazdpaginated');
        $controller->get('/usun/{id}', [$this, 'usunAction'])
            ->assert('id', '[1-9][0-9]*')
            ->bind('adminwjazdusun');
        $controller->match('/edytuj/{id}', [$this, 'edytujAction'])
            ->method('POST|GET')
            ->assert('id', '[1-9][0-9]*')
            ->bind('adminwjazdedytuj');


        return $controller;
    }

    /**
     * Index action.
     *
     * @param \Silex\Application $app
     * @param int                $page
     *
     * @return mixed
     */
    public function indexAction(Application $app, $page = 1)
    {
        $wjazdRepository = new WjazdRepository($app['db']);
        $id = $this->czyIKtoraFirmaIParking($app);

        $dane = $wjazdRepository->pobierzPaginated($page, $id[0], $id[1]);
        if (1 !== $page && (!$dane['data'] || !count($dane['data']))) {
            return $app->redirect($app['url_generator']->generate('adminwjazd'));
        }

        return $app['twig']->render(
            'AdminWjazd/adminwjazd.html.twig',
            ['paginator' => $dane]
        );
    }

    /**
     * Edytuj action.
     *
     * @param \Silex\Application                        $app
     * @param int                                       $id
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function edytujAction(Application $app, $id, Request $request)
    {
        $tag = $this->wypelnijParking($app);

        $wjazdRepository = new WjazdRepository($app['db']);
        $wjazd = $wjazdRepository->pobierzDaneJednego($id);

        $idFirmaIParking = $this->czyIKtoraFirmaIParking($app);
        $idFirma = $idFirmaIParking[0];
        $idParking = $idFirmaIParking[1];
        if ((-1 !== $idFirma && $wjazd['FK_firma'] !== $idFirma) || (-1 !== $idParking && $wjazd['ID_parking'] !== $idParking)) {
            $app['session']->getFlashBag()->add(
                'messages',
                [
                    'type' => 'danger',
                    'message' => 'wiadomosc.niemaszprawa',
                ]
            );

            return $app->redirect($app['url_generator']->generate('adminwjazd'), 301);
        }

        if (!$wjazd['ID_parking']) {
            $app['session']->getFlashBag()->add(
                'messages',
                [
                'type' => 'danger',
                'message' => 'wiadomosc.rekordnieistnieje',
                ]
            );

            return $app->redirect($app['url_generator']->generate('adminwjazd'));
        }

        $nazwy = ['nr_biletu' => 'nr_biletu', 'parking_nazwa' => 'FK_parking', 'koszt' => 'koszt',
            'czas_wjazdu' => 'czas_wjazdu', 'czas_wyjazdu' => 'czas_wyjazdu', ];
        foreach ($nazwy as $klucz => $wartosc) {
            $tag['dane'][$klucz] = $wjazd[$wartosc];
        }

        $form = $app['form.factory']->createBuilder(AdminWjazdType::class, $tag)->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $dane = $form->getData();
            $wjazdRepository = new WjazdRepository($app['db']);
            $wjazdRepository->edytuj($dane, $id);

            $app['session']->getFlashBag()->add(
                'messages',
                [
                    'type' => 'success',
                    'message' => 'wiadomosc.edycja',
                ]
            );

            return $app->redirect($app['url_generator']->generate('adminwjazd'), 301);
        }

        return $app['twig']->render(
            'AdminWjazd/adminwjazdedytuj.html.twig',
            [
                'id' => $id,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * Usuń action
     *
     * @param \Silex\Application $app
     * @param  int                $id
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @throws \Doctrine\DBAL\Exception\InvalidArgumentException
     */
    public function usunAction(Application $app, $id)
    {
        $wjazdRepository = new WjazdRepository($app['db']);
        $wjazd = $wjazdRepository->pobierzDaneJednego($id);

        $idFirmaIParking = $this->czyIKtoraFirmaIParking($app);
        $idFirma = $idFirmaIParking[0];
        $idParking = $idFirmaIParking[1];
        if ((-1 !== $idFirma && $wjazd['FK_firma'] !== $idFirma) || (-1 !== $idParking && $wjazd['ID_parking'] !== $idParking)) {
            $app['session']->getFlashBag()->add(
                'messages',
                [
                    'type' => 'danger',
                    'message' => 'wiadomosc.niemaszprawa',
                ]
            );

            return $app->redirect($app['url_generator']->generate('adminwjazd'), 301);
        }

        if ($wjazdRepository->usun($id) === 0) {
            $app['session']->getFlashBag()->add(
                'messages',
                [
                    'type' => 'danger',
                    'message' => 'wiadomosc.wjazdNieIstnieje',
                ]
            );
        } else {
            $app['session']->getFlashBag()->add(
                'messages',
                [
                    'type' => 'success',
                    'message' => 'wiadomosc.usuniecie',
                ]
            );
        };


        return $app->redirect($app['url_generator']->generate('adminwjazd'), 301);
    }

    /**
     * Zwraca idFirmy jeśli nie admin (lub -1) i idParking
     *
     * @param \Silex\Application $app
     *
     * @return array
     */
    private function czyIKtoraFirmaIParking(Application $app)
    {
        $idFirmy = -1;
        $idParking = -1;
        if (!$app['security.authorization_checker']->isGranted('ROLE_ADMIN')) {
            $login = $app['security.token_storage']->getToken()->getUser()->getUsername();
            $pracownikRepository = new PracownikRepository($app['db']);
            $dane = $pracownikRepository->pobierzIDFirmyIParkingu($login);
            $idFirmy = $dane['FK_firma'];
            if (!$app['security.authorization_checker']->isGranted('ROLE_FIRMA')) {
                $idParking = $dane['FK_parking'];
            }
        }

        return [$idFirmy, $idParking];
    }

    /**
     * Wypełnij ChoiceType z parkingu.
     *
     * @param \Silex\Application $app
     *
     * @return array
     */
    private function wypelnijParking(Application $app)
    {
        $tag = [];

        $parkingRepository = new ParkingRepository($app['db']);
        $parkingi = $parkingRepository->pobierzWszystko();
        $id = $this->czyIKtoraFirmaIParking($app);
        $idFirma = $id[0];
        if ($app['security.authorization_checker']->isGranted('ROLE_FIRMA')) {
            foreach ($parkingi as $parking) {
                if ($app['security.authorization_checker']->isGranted('ROLE_ADMIN') || $idFirma === $parking['FK_firma']) {
                    $tag['dane']['parking'][$parking['parking_nazwa']] = $parking['ID_parking'];
                }
            }
        }

        return $tag;
    }
}
