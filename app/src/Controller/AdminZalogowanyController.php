<?php
/**
 * AdminZalogowany controller
 */
namespace Controller;

use Form\AdminZalogowanyType;
use Repository\PracownikRepository;
use Silex\Application;
use Silex\Api\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdminZalogowanyController
 */
class AdminZalogowanyController implements ControllerProviderInterface
{
    /**
     * Routing settings.
     *
     * @param \Silex\Application $app Silex application
     *
     * @return \Silex\ControllerCollection Result
     */
    public function connect(Application $app)
    {
        $controller = $app['controllers_factory'];
        $controller->match('/', [$this, 'indexAction'])
            ->method('GET|POST')
            ->bind('adminzalogowany');

        return $controller;
    }

    /**
     * Index action
     *
     * @param \Silex\Application                        $app
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function indexAction(Application $app, Request $request)
    {
        $login = $app['security.token_storage']->getToken()->getUser()->getUsername();
        $pracownikRepository = new PracownikRepository($app['db']);
        $idFirmyIParkingu = $pracownikRepository->pobierzIDFirmyIParkingu($login);
        $id = $idFirmyIParkingu['ID_pracownik'];

        $pracownik = $pracownikRepository->pobierzDaneJednego($id);

        $nazwy = ['login', 'email'];
        foreach ($nazwy as $nazwa) {
            $tag['dane'][$nazwa] = $pracownik[$nazwa];
        }
        $tag['dane']['id'] = $id;

        $form = $app['form.factory']->createBuilder(AdminZalogowanyType::class, $tag, ['pracownik_repository' => new PracownikRepository($app['db'])])->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $dane = $form->getData();
            $pracownikRepository = new PracownikRepository($app['db']);
            if ($dane['haslo']) {
                $dane['haslo'] = $app['security.encoder.bcrypt']->encodePassword($dane['haslo'], '');
            }
            $pracownikRepository->edytujSiebie($dane, $id);

            $app['session']->getFlashBag()->add(
                'messages',
                [
                    'type' => 'success',
                    'message' => 'wiadomosc.edycja',
                ]
            );

            return $app->redirect($app['url_generator']->generate('adminpanel'));
        }

        return $app['twig']->render(
            'AdminZalogowany/adminzalogowany.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }
}
