<?php
/**
 * AdminPracownik controller
 */
namespace Controller;

use Repository\ParkingRepository;
use Silex\Application;
use Silex\Api\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Repository\PracownikRepository;
use Form\AdminPracownikType;
use Repository\RolaRepository;
use Repository\FirmaRepository;

/**
 * Class AdminPracownikController
 */
class AdminPracownikController implements ControllerProviderInterface
{
    /**
     * Routing settings.
     *
     * @param \Silex\Application $app Silex application
     *
     * @return \Silex\ControllerCollection Result
     */
    public function connect(Application $app)
    {
        $controller = $app['controllers_factory'];
        $controller->get('/', [$this, 'indexAction'])
            ->bind('adminpracownik');
        $controller->match('/dodaj', [$this, 'dodajAction'])
            ->method('POST|GET')
            ->bind('adminpracownikdodaj');
        $controller->get('/usun/{id}', [$this, 'usunAction'])
            ->assert('id', '[1-9][0-9]*')
            ->bind('adminpracownikusun');
        $controller->match('/edytuj/{id}', [$this, 'edytujAction'])
            ->method('POST|GET')
            ->assert('id', '[1-9][0-9]*')
            ->bind('adminpracownikedytuj');
        $controller->get('/page/{page}', [$this, 'indexAction'])
            ->value('page', 1)
            ->bind('adminpracownikpaginated');

        return $controller;
    }

    /**
     * Index action.
     *
     * @param \Silex\Application $app
     * @param int                $page
     *
     * @return mixed
     */
    public function indexAction(Application $app, $page = 1)
    {
        $pracownikRepository = new PracownikRepository($app['db']);
        $id = $this->czyIKtoraFirma($app);

        $dane = $pracownikRepository->pobierzPaginated($page, $id);
        if (1 !== $page && (!$dane['data'] || !count($dane['data']))) {
            return $app->redirect($app['url_generator']->generate('adminpracownik'));
        }

        return $app['twig']->render(
            'AdminPracownik/adminpracownik.html.twig',
            ['paginator' => $dane]
        );
    }

    /**
     * Dodaj action.
     *
     * @param \Silex\Application                        $app
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function dodajAction(Application $app, Request $request)
    {
        $tag = $this->wypelnijFirmeRoleIParking($app);

        $form = $app['form.factory']->createBuilder(AdminPracownikType::class, $tag, ['pracownik_repository' => new PracownikRepository($app['db'])])->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $dane = $form->getData();
            $pracownikRepository = new PracownikRepository($app['db']);
            $dane['haslo'] = $app['security.encoder.bcrypt']->encodePassword($dane['haslo'], '');

            $rolaRepository = new RolaRepository($app['db']);
            $roleAdmin = $rolaRepository->pobierzRolaAdmin();
            $roleFirma = $rolaRepository->pobierzRolaFirma();
            if ($dane['rola'] === $roleAdmin) {
                unset($dane['parking']);
                unset($dane['firma']);
            }
            if ($dane['rola'] === $roleFirma) {
                unset($dane['parking']);
            }

            $pracownikRepository->dodaj($dane);

            $app['session']->getFlashBag()->add(
                'messages',
                [
                    'type' => 'success',
                    'message' => 'wiadomosc.dodanie',
                ]
            );

            return $app->redirect($app['url_generator']->generate('adminpracownik'));
        }

        return $app['twig']->render(
            'AdminPracownik/adminpracownikdodaj.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }



    /**
     * Edytuj action.
     *
     * @param \Silex\Application                        $app
     * @param int                                       $id
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function edytujAction(Application $app, $id, Request $request)
    {
        $tag = $this->wypelnijFirmeRoleIParking($app);

        $pracownikRepository = new PracownikRepository($app['db']);
        $pracownik = $pracownikRepository->pobierzDaneJednego($id);
        $idFirma = $this->czyIKtoraFirma($app);
        if (-1 !== $idFirma && $pracownik['FK_firma'] !== $idFirma) {
            $app['session']->getFlashBag()->add(
                'messages',
                [
                    'type' => 'danger',
                    'message' => 'wiadomosc.niemaszprawa',
                ]
            );

            return $app->redirect($app['url_generator']->generate('adminpracownik'), 301);
        }

        if (!$pracownik['ID_pracownik']) {
            $app['session']->getFlashBag()->add(
                'messages',
                [
                'type' => 'danger',
                'message' => 'wiadomosc.rekordnieistnieje',
                ]
            );

            return $app->redirect($app['url_generator']->generate('adminpracownik'));
        }

        $nazwy = ['login' => 'login', 'imie' => 'imie', 'nazwisko' => 'nazwisko', 'email' => 'email',
            'firma_wartosc' => 'FK_firma', 'rola_wartosc' => 'FK_rola', 'parking_wartosc' => 'FK_parking', 'id' => 'ID_pracownik', ];
        foreach ($nazwy as $klucz => $wartosc) {
            $tag['dane'][$klucz] = $pracownik[$wartosc];
        }

        $form = $app['form.factory']->createBuilder(AdminPracownikType::class, $tag, ['pracownik_repository' => new PracownikRepository($app['db'])])->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $dane = $form->getData();
            $pracownikRepository = new PracownikRepository($app['db']);
            if ($dane['haslo']) {
                $dane['haslo'] = $app['security.encoder.bcrypt']->encodePassword($dane['haslo'], '');
            }

            $rolaRepository = new RolaRepository($app['db']);
            $roleAdmin = $rolaRepository->pobierzRolaAdmin();
            $roleFirma = $rolaRepository->pobierzRolaFirma();
            if ($dane['rola'] === $roleAdmin) {
                unset($dane['parking']);
                unset($dane['firma']);
            }
            if ($dane['rola'] === $roleFirma) {
                unset($dane['parking']);
            }

            $pracownikRepository->edytuj($dane, $id);

            $app['session']->getFlashBag()->add(
                'messages',
                [
                    'type' => 'success',
                    'message' => 'wiadomosc.edycja',
                ]
            );

            return $app->redirect($app['url_generator']->generate('adminpracownik'), 301);
        }

        return $app['twig']->render(
            'AdminPracownik/adminpracownikedytuj.html.twig',
            [
                'id' => $id,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * Usuń action
     *
     * @param \Silex\Application $app
     * @param int                $id
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function usunAction(Application $app, $id)
    {
        $pracownikRepository = new PracownikRepository($app['db']);
        $pracownik = $pracownikRepository->pobierzDaneJednego($id);

        $idFirma = $this->czyIKtoraFirma($app);
        if (-1 !== $idFirma && $pracownik['FK_firma'] !== $idFirma) {
            $app['session']->getFlashBag()->add(
                'messages',
                [
                    'type' => 'danger',
                    'message' => 'wiadomosc.niemaszprawa',
                ]
            );

            return $app->redirect($app['url_generator']->generate('adminparking'), 301);
        }

        if ($pracownikRepository->usun($id) === 0) {
            $app['session']->getFlashBag()->add(
                'messages',
                [
                    'type' => 'danger',
                    'message' => 'wiadomosc.pracownikNieIstnieje',
                ]
            );
        } else {
            $app['session']->getFlashBag()->add(
                'messages',
                [
                    'type' => 'success',
                    'message' => 'wiadomosc.usuniecie',
                ]
            );
        }


        return $app->redirect($app['url_generator']->generate('adminpracownik'), 301);
    }

    /**
     * Zwróć idFirmy jeżeli nie jesteśmy adminem lub -1.
     *
     * @param \Silex\Application $app
     *
     * @return int
     */
    private function czyIKtoraFirma(Application $app)
    {
        $idFirmy = -1;
        if (!$app['security.authorization_checker']->isGranted('ROLE_ADMIN')) {
            $login = $app['security.token_storage']->getToken()->getUser()->getUsername();
            $pracownikRepository = new PracownikRepository($app['db']);
            $dane = $pracownikRepository->pobierzIDFirmyIParkingu($login);
            $idFirmy = $dane['FK_firma'];
        }

        return $idFirmy;
    }

    /**
     * Wypełnij ChoiceType firmy, roli i parkingu.
     *
     * @param \Silex\Application $app
     *
     * @return array
     */
    private function wypelnijFirmeRoleIParking(Application $app)
    {
        $tag = [];

        $idFirmy = $this->czyIKtoraFirma($app);

        $rolaRepository = new RolaRepository($app['db']);
        $role = $rolaRepository->pobierz();
        $rolaAdmin = $rolaRepository->pobierzRolaAdmin();
        foreach ($role as $rola) {
            if ($app['security.authorization_checker']->isGranted('ROLE_ADMIN') || $rola['ID_rola'] !== $rolaAdmin) {
                $tag['dane']['rola'][$rola['nazwa']] = $rola['ID_rola'];
            }
        }

        if ($app['security.authorization_checker']->isGranted('ROLE_ADMIN')) {
            $firmaRepository = new FirmaRepository($app['db']);
            $firmy = $firmaRepository->pobierzWszystko();
            foreach ($firmy as $firma) {
                $tag['dane']['firma'][$firma['nazwa']] = $firma['ID_firma'];
            }
        } else {
            $tag['firma'] = $idFirmy;
        }

        $parkingRepository = new ParkingRepository($app['db']);
        $parkingi = $parkingRepository->pobierzWszystko();
        foreach ($parkingi as $parking) {
            if ($app['security.authorization_checker']->isGranted('ROLE_ADMIN') || $parking['FK_firma'] === $idFirmy) {
                $tag['dane']['parking'][$parking['parking_nazwa']] = $parking['ID_parking'];
            }
        }

        return $tag;
    }
}
