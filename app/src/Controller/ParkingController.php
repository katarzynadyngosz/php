<?php
/**
 * ParkingController
 */
namespace Controller;

use Silex\Application;
use Silex\Api\ControllerProviderInterface;
use Repository\ParkingRepository;

/**
 * Class ParkingController
 */
class ParkingController implements ControllerProviderInterface
{
    /**
     * Routing settings.
     *
     * @param \Silex\Application $app Silex application
     *
     * @return \Silex\ControllerCollection Result
     */
    public function connect(Application $app)
    {
        $controller = $app['controllers_factory'];
        $controller->get('/', [$this, 'indexAction'])
            ->bind('parking');

        return $controller;
    }

    /**
     * Index action.
     *
     * @param \Silex\Application $app Silex application
     *
     * @return string Response
     */
    public function indexAction(Application $app)
    {
        $parkingRepository = new ParkingRepository($app['db']);
        $dane = $parkingRepository->pobierzWszystko();

        return $app['twig']->render(
            'Parking/parking.html.twig',
            ['dane' => $dane]
        );
    }
}
