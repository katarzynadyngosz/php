<?php
/**
 * AdminParking controller
 */
namespace Controller;

use Repository\PracownikRepository;
use Silex\Application;
use Silex\Api\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Repository\FirmaRepository;
use Form\AdminParkingType;
use Repository\ParkingRepository;

/**
 * Class AdminParkingController
 */
class AdminParkingController implements ControllerProviderInterface
{
    /**
     * Routing settings.
     *
     * @param \Silex\Application $app Silex application
     *
     * @return \Silex\ControllerCollection Result
     */
    public function connect(Application $app)
    {
        $controller = $app['controllers_factory'];
        $controller->get('/', [$this, 'indexAction'])
            ->bind('adminparking');
        $controller->match('/dodaj', [$this, 'dodajAction'])
            ->method('POST|GET')
            ->bind('adminparkingdodaj');
        $controller->get('/usun/{id}', [$this, 'usunAction'])
            ->assert('id', '[1-9][0-9]*')
            ->bind('adminparkingusun');
        $controller->match('/edytuj/{id}', [$this, 'edytujAction'])
            ->method('POST|GET')
            ->assert('id', '[1-9][0-9]*')
            ->bind('adminparkingedytuj');
        $controller->get('/page/{page}', [$this, 'indexAction'])
            ->value('page', 1)
            ->bind('adminparkingpaginated');

        return $controller;
    }

    /**
     * Index action.
     *
     * @param \Silex\Application $app
     * @param int                $page
     *
     * @return mixed
     */
    public function indexAction(Application $app, $page = 1)
    {
        $parkingRepository = new ParkingRepository($app['db']);
        $id = $this->czyIKtoraFirma($app);

        $dane = $parkingRepository->pobierzPaginated($page, $id);
        if (1 !== $page && (!$dane['data'] || !count($dane['data']))) {
            return $app->redirect($app['url_generator']->generate('adminparking'));
        }

        return $app['twig']->render(
            'AdminParking/adminparking.html.twig',
            ['paginator' => $dane]
        );
    }



    /**
     * Dodaj action.
     *
     * @param \Silex\Application                        $app
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function dodajAction(Application $app, Request $request)
    {
        $tag = $this->wypelnijFirme($app);

        $form = $app['form.factory']->createBuilder(AdminParkingType::class, $tag, ['parking_repository' => new ParkingRepository($app['db'])])->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $dane = $form->getData();
            $parkingRepository = new ParkingRepository($app['db']);
            $parkingRepository->dodaj($dane);

            $app['session']->getFlashBag()->add(
                'messages',
                [
                    'type' => 'success',
                    'message' => 'wiadomosc.dodanie',
                ]
            );

            return $app->redirect($app['url_generator']->generate('adminparking'));
        }

        return $app['twig']->render(
            'AdminParking/adminparkingdodaj.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * Edytuj action.
     *
     * @param \Silex\Application                        $app
     * @param int                                       $id
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function edytujAction(Application $app, $id, Request $request)
    {
        $tag = $this->wypelnijFirme($app);

        $parkingRepository = new ParkingRepository($app['db']);
        $parking = $parkingRepository->pobierzDaneJednego($id);

        $idFirma = $this->czyIKtoraFirma($app);
        if (-1 !== $idFirma && $parking['FK_firma'] !== $idFirma) {
            $app['session']->getFlashBag()->add(
                'messages',
                [
                    'type' => 'danger',
                    'message' => 'wiadomosc.niemaszprawa',
                ]
            );

            return $app->redirect($app['url_generator']->generate('adminparking'), 301);
        }

        if (!$parking['FK_firma']) {
            $app['session']->getFlashBag()->add(
                'messages',
                [
                'type' => 'danger',
                'message' => 'wiadomosc.rekordnieistnieje',
                ]
            );

            return $app->redirect($app['url_generator']->generate('adminparking'));
        }

        $nazwy = ['liczba_miejsc' => 'liczba_miejsc', 'kwota_godzina' => 'kwota_godzina', 'parking_nazwa' => 'parking_nazwa',
            'firma_nazwa' => 'FK_firma', ];
        foreach ($nazwy as $klucz => $wartosc) {
            $tag['dane'][$klucz] = $parking[$wartosc];
        }
        $tag['dane']['id'] = $id;

        $form = $app['form.factory']->createBuilder(AdminParkingType::class, $tag, ['parking_repository' => new ParkingRepository($app['db'])])->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $dane = $form->getData();
            $parkingRepository = new ParkingRepository($app['db']);
            $parkingRepository->edytuj($dane, $id);

            $app['session']->getFlashBag()->add(
                'messages',
                [
                    'type' => 'success',
                    'message' => 'wiadomosc.edycja',
                ]
            );

            return $app->redirect($app['url_generator']->generate('adminparking'), 301);
        }

        return $app['twig']->render(
            'AdminParking/adminparkingedytuj.html.twig',
            [
                'id' => $id,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * Usuń action
     *
     * @param \Silex\Application $app
     * @param int                $id
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function usunAction(Application $app, $id)
    {
        $parkingRepository = new ParkingRepository($app['db']);
        $parking = $parkingRepository->pobierzDaneJednego($id);

        $idFirma = $this->czyIKtoraFirma($app);
        if (-1 !== $idFirma && $parking['FK_firma'] !== $idFirma) {
            $app['session']->getFlashBag()->add(
                'messages',
                [
                    'type' => 'danger',
                    'message' => 'wiadomosc.niemaszprawa',
                ]
            );

            return $app->redirect($app['url_generator']->generate('adminparking'), 301);
        }

        if ($parkingRepository->usun($id) === 0) {
            $app['session']->getFlashBag()->add(
                'messages',
                [
                    'type' => 'danger',
                    'message' => 'wiadomosc.parkingNieIstnieje',
                ]
            );
        } else {
            $app['session']->getFlashBag()->add(
                'messages',
                [
                    'type' => 'success',
                    'message' => 'wiadomosc.usuniecie',
                ]
            );
        };

        return $app->redirect($app['url_generator']->generate('adminparking'), 301);
    }

    /**
     * Zwróć idFirmy jeżeli nie jesteśmy adminem lub -1.
     *
     * @param \Silex\Application $app
     *
     * @return int
     */
    private function czyIKtoraFirma(Application $app)
    {
        $idFirmy = -1;
        if (!$app['security.authorization_checker']->isGranted('ROLE_ADMIN')) {
            $login = $app['security.token_storage']->getToken()->getUser()->getUsername();
            $pracownikRepository = new PracownikRepository($app['db']);
            $dane = $pracownikRepository->pobierzIDFirmyIParkingu($login);
            $idFirmy = $dane['FK_firma'];
        }

        return $idFirmy;
    }

    /**
     * Wypełnij ChoiceType z danymi firm.
     *
     * @param \Silex\Application $app
     *
     * @return array
     */
    private function wypelnijFirme(Application $app)
    {
        //jeżeli admin to pobierz wszystko, jeżeli nie to wstaw id swojej firmy
        $tag = [];
        $idFirmy = $this->czyIKtoraFirma($app);
        if ($app['security.authorization_checker']->isGranted('ROLE_ADMIN')) {
            $firmaRepository = new FirmaRepository($app['db']);
            $firmy = $firmaRepository->pobierzWszystko();
            foreach ($firmy as $firma) {
                $tag['dane']['firma'][$firma['nazwa']] = $firma['ID_firma'];
            }
        } else {
            $tag['firma'] = $idFirmy;
        }

        return $tag;
    }
}
