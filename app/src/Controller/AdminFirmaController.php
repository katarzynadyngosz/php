<?php
/**
 * AdminFirma controller
 */
namespace Controller;

use Silex\Application;
use Silex\Api\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Repository\FirmaRepository;
use Form\AdminFirmaType;

/**
 * Class AdminFirmaController
 */
class AdminFirmaController implements ControllerProviderInterface
{
    /**
     * Routing settings.
     *
     * @param \Silex\Application $app Silex application
     *
     * @return \Silex\ControllerCollection Result
     */
    public function connect(Application $app)
    {
        $controller = $app['controllers_factory'];
        $controller->get('/', [$this, 'indexAction'])
            ->bind('adminfirma');
        $controller->match('/dodaj', [$this, 'dodajAction'])
            ->method('POST|GET')
            ->bind('adminfirmadodaj');
        $controller->get('/usun/{id}', [$this, 'usunAction'])
            ->assert('id', '[1-9][0-9]*')
            ->bind('adminfirmausun');
        $controller->match('/edytuj/{id}', [$this, 'edytujAction'])
            ->method('POST|GET')
            ->assert('id', '[1-9][0-9]*')
            ->bind('adminfirmaedytuj');
        $controller->get('/page/{page}', [$this, 'indexAction'])
            ->value('page', 1)
            ->bind('adminfirmapaginated');

        return $controller;
    }

    /**
     * Index action.
     *
     * @param \Silex\Application $app
     * @param int                $page
     *
     * @return mixed
     */
    public function indexAction(Application $app, $page = 1)
    {
        $firmaRepository = new FirmaRepository($app['db']);
        $dane = $firmaRepository->pobierzPaginated($page);
        if (1 !== $page && (!$dane['data'] || !count($dane['data']))) {
            return $app->redirect($app['url_generator']->generate('adminfirma'));
        }

        return $app['twig']->render(
            'AdminFirma/adminfirma.html.twig',
            ['paginator' => $dane]
        );
    }

    /**
     * Dodaj action.
     *
     * @param Application $app
     * @param Request     $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function dodajAction(Application $app, Request $request)
    {
        $tag = [];

        $form = $app['form.factory']->createBuilder(AdminFirmaType::class, $tag, ['firma_repository' => new FirmaRepository($app['db'])])->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $dane = $form->getData();
            $firmaRepository = new FirmaRepository($app['db']);
            $firmaRepository->dodaj($dane);

            $app['session']->getFlashBag()->add(
                'messages',
                [
                    'type' => 'success',
                    'message' => 'wiadomosc.dodanie',
                ]
            );

            return $app->redirect($app['url_generator']->generate('adminfirma'));
        }

        return $app['twig']->render(
            'AdminFirma/adminfirmadodaj.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * Edytuj action
     *
     * @param Application $app
     * @param int         $id
     * @param Request     $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function edytujAction(Application $app, $id, Request $request)
    {
        $firmaRepository = new FirmaRepository($app['db']);
        $firma = $firmaRepository->pobierzDaneJednego($id);

        $nazwy = ['nazwa', 'email', 'nip', 'miasto'];
        foreach ($nazwy as $nazwa) {
            $tag['dane'][$nazwa] = $firma[$nazwa];
        }
        $tag['dane']['id'] = $id;

        if (!$id) {
            $app['session']->getFlashBag()->add(
                'messages',
                [
                'type' => 'danger',
                'message' => 'wiadomosc.rekordnieistnieje',
                ]
            );

            return $app->redirect($app['url_generator']->generate('adminfirma'));
        }

        $form = $app['form.factory']->createBuilder(AdminFirmaType::class, $tag, ['firma_repository' => new FirmaRepository($app['db'])])->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $dane = $form->getData();
            $firmaRepository = new FirmaRepository($app['db']);
            $firmaRepository->edytuj($dane, $id);

            $app['session']->getFlashBag()->add(
                'messages',
                [
                    'type' => 'success',
                    'message' => 'wiadomosc.edycja',
                ]
            );

            return $app->redirect($app['url_generator']->generate('adminfirma'), 301);
        }

        return $app['twig']->render(
            'AdminFirma/adminfirmaedytuj.html.twig',
            [
                'id' => $id,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * Usuń action
     *
     * @param Application $app
     * @param int         $id
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function usunAction(Application $app, $id)
    {
        $firmaRepository = new FirmaRepository($app['db']);

        if ($firmaRepository->usun($id) === 0) {
            $app['session']->getFlashBag()->add(
                'messages',
                [
                    'type' => 'danger',
                    'message' => 'wiadomosc.firmaNieIstnieje',
                ]
            );
        } else {
            $app['session']->getFlashBag()->add(
                'messages',
                [
                    'type' => 'success',
                    'message' => 'wiadomosc.usuniecie',
                ]
            );
        };


        return $app->redirect($app['url_generator']->generate('adminfirma'), 301);
    }
}
