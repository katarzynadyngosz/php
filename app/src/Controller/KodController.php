<?php
/**
 * Kod controller
 */
namespace Controller;

use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Silex\Application;
use Silex\Api\ControllerProviderInterface;
use Repository\WjazdRepository;

/**
 * Class KodController
 */
class KodController implements ControllerProviderInterface
{
    /**
     * Routing settings.
     *
     * @param \Silex\Application $app Silex application
     *
     * @return \Silex\ControllerCollection Result
     */
    public function connect(Application $app)
    {
        $controller = $app['controllers_factory'];
        $controller->get('/{idParking}', [$this, 'indexAction'])
            ->assert('idParking', '[1-9][0-9]*')
            ->bind('kod');

        return $controller;
    }

    /**
     * Index action.
     *
     * @param \Silex\Application $app
     * @param int                $idParking
     *
     * @return mixed
     */
    public function indexAction(Application $app, $idParking)
    {
        $kod = uniqid();

        $wjazdRepository = new WjazdRepository($app['db']);
        try {
            $wjazdRepository->dodaj($kod, $idParking);
        } catch (ForeignKeyConstraintViolationException $exception) {
            $app['session']->getFlashBag()->add(
                'messages',
                [
                    'type' => 'danger',
                    'message' => 'wiadomosc.parkingNieIstnieje',
                ]
            );

            return $app->redirect($app['url_generator']->generate('parking'));
        }

        return $app['twig']->render(
            'Kod/kod.html.twig',
            ['id' => $idParking,
            'kod' => $kod, ]
        );
    }
}
