<?php
/**
 * Unique Tag constraint.
 */
namespace Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Class UniqueTag.
 */
class UniqueNazwa extends Constraint
{
    /**
     * Message.
     *
     * @var string $message
     */
    public $message = 'walidator.nazwa';

    /**
     * Element id.
     *
     * @var int|string|null $elementId
     */
    public $elementId = null;

    /**
     * Tag repository.
     *
     * @var null|\Repository $repository
     */
    public $repository = null;
}
