<?php
/**
 * Pracownik repository
 */
namespace Repository;

use Doctrine\DBAL\Connection;
use Utils\Paginator;
use Doctrine\DBAL\DBALException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;

/**
 * Class PracownikRepository.
 */
class PracownikRepository
{
    /**
     * Doctrine DBAL connection.
     *
     * @var \Doctrine\DBAL\Connection $db
     */
    protected $db;

    /**
     * Number of items per page.
     *
     * const int NUM_ITEMS
     */
    const NUM_ITEMS = 5;

    /**
     * PracownikRepository constructor.
     *
     * @param \Doctrine\DBAL\Connection $db
     */
    public function __construct(Connection $db)
    {
        $this->db = $db;
    }

    /**
     * Edytuj pracownika o danym id
     *
     * @param array $dane
     * @param int   $id
     */
    public function edytuj($dane, $id)
    {
        $pracownik = [];
        $pracownikNazwy = ['login' => 'login', 'FK_firma' => 'firma', 'haslo' => 'haslo', 'FK_rola' => 'rola', 'FK_parking' => 'parking'];
        foreach ($pracownikNazwy as $klucz => $wartosc) {
            if (isset($dane[$wartosc])) {
                $pracownik[$klucz] = $dane[$wartosc];
            }
        }

        $danePracownika = [];
        $danePracownikaNazwy = ['imie', 'nazwisko', 'email'];
        foreach ($danePracownikaNazwy as $komorka) {
            $danePracownika[$komorka] = $dane[$komorka];
        }

        $this->db->update('Pracownik', $pracownik, ['ID_pracownik' => $id]);

        $this->db->update('Dane_pracownika', $danePracownika, ['FK_pracownik' => $id]);
    }

    /**
     * Edytuj siebie
     *
     * @param array $dane
     * @param int   $id
     */
    public function edytujSiebie($dane, $id)
    {
        $pracownik = [];
        $pracownikNazwy = ['login', 'haslo'];
        foreach ($pracownikNazwy as $nazwa) {
            if (isset($dane[$nazwa])) {
                $pracownik[$nazwa] = $dane[$nazwa];
            }
        }


        $danePracownika = [];
        $danePracownikaNazwy = ['email'];
        foreach ($danePracownikaNazwy as $komorka) {
            $danePracownika[$komorka] = $dane[$komorka];
        }

        $this->db->update('Pracownik', $pracownik, ['ID_pracownik' => $id]);

        $this->db->update('Dane_pracownika', $danePracownika, ['FK_pracownik' => $id]);
    }

    /**
     * Dodaj pracownika o danym id
     *
     * @param array $dane
     */
    public function dodaj($dane)
    {

        $pracownik = [];
        $pracownikNazwy = ['login' => 'login', 'FK_firma' => 'firma', 'haslo' => 'haslo', 'FK_rola' => 'rola', 'FK_parking' => 'parking'];
        foreach ($pracownikNazwy as $klucz => $wartosc) {
            if (isset($dane[$wartosc])) {
                $pracownik[$klucz] = $dane[$wartosc];
            }
        }

        $danePracownika = [];
        $danePracownikaNazwy = ['imie', 'nazwisko', 'email'];
        foreach ($danePracownikaNazwy as $komorka) {
            $danePracownika[$komorka] = $dane[$komorka];
        }

        $this->db->insert('Pracownik', $pracownik);
        $id = $this->db->lastInsertId();

        $danePracownika['FK_pracownik'] = $id;
        $this->db->insert('Dane_pracownika', $danePracownika);
    }

    /**
     * Pobierz wszystkie dane pracowników
     *
     * @return array
     */
    public function pobierzDaneWszystkich()
    {
        $queryBuilder = $this->pobierz();

        $result = $queryBuilder->execute()->fetchAll();

        return $result;
    }


    /**
     * Pobierz dane pracownika o danym id
     *
     * @param int $id
     *
     * @return mixed
     */
    public function pobierzDaneJednego($id)
    {
        $queryBuilder = $this->pobierz();

        $queryBuilder->where('p.ID_pracownik = :id')
            ->setParameter(':id', $id, \PDO::PARAM_INT);

        $result = $queryBuilder->execute()->fetch();

        return $result;
    }

    /**
     * Usuwa pracownika o danym id
     *
     * @param int $id
     *
     * @return int
     *
     * @throws \Doctrine\DBAL\Exception\InvalidArgumentException
     */
    public function usun($id)
    {
        $this->db->delete('Dane_pracownika', ['FK_pracownik' => $id]);

        return $this->db->delete('Pracownik', ['ID_pracownik' => $id]);
    }

    /**
     * Pobierz dane zpaginowane z ewentualnym id firmy.
     *
     * @param int $page
     * @param int $idFirma
     *
     * @return array
     */
    public function pobierzPaginated($page, $idFirma)
    {
        $countQueryBuilder = $this->pobierz($idFirma)
            ->select('COUNT(p.ID_pracownik) AS total_results')
            ->setMaxResults(1);

        $paginator = new Paginator($this->pobierz($idFirma), $countQueryBuilder);
        $paginator->setCurrentPage($page);
        $paginator->setMaxPerPage(self::NUM_ITEMS);

        return $paginator->getCurrentPageResults();
    }

    /**
     * Loads user by login.
     *
     * @param string $login User login
     *
     * @throws UsernameNotFoundException
     *
     * @return array Result
     */
    public function loadUserByLogin($login)
    {
        try {
            $user = $this->getUserByLogin($login);

            if (!$user || !count($user)) {
                throw new UsernameNotFoundException(
                    sprintf('Username "%s" does not exist.', $login)
                );
            }


            $roles = $this->getUserRoles($user['ID_pracownik']);

            if (!$roles || !count($roles)) {
                throw new UsernameNotFoundException(
                    sprintf('Username "%s" does not exist.', $login)
                );
            }

            return [
                'login' => $user['login'],
                'password' => $user['haslo'],
                'roles' => $roles,
            ];
        } catch (DBALException $exception) {
            throw new UsernameNotFoundException(
                sprintf('Username "%s" does not exist.', $login)
            );
        } catch (UsernameNotFoundException $exception) {
            throw $exception;
        }
    }

    /**
     * Gets user data by login.
     *
     * @param string $login User login
     *
     * @return array Result
     */
    public function getUserByLogin($login)
    {
        try {
            $queryBuilder = $this->db->createQueryBuilder();
            $queryBuilder->select('u.ID_pracownik', 'u.login', 'u.haslo')
                ->from('Pracownik', 'u')
                ->where('u.login = :login')
                ->setParameter(':login', $login, \PDO::PARAM_STR);

            return $queryBuilder->execute()->fetch();
        } catch (DBALException $exception) {
            return [];
        }
    }

    /**
     * Gets user roles by User ID.
     *
     * @param integer $userId User ID
     *
     * @return array Result
     */
    public function getUserRoles($userId)
    {
        $roles = [];

        try {
            $queryBuilder = $this->db->createQueryBuilder();
            $queryBuilder->select('r.nazwa')
                ->from('Pracownik', 'u')
                ->innerJoin('u', 'Rola', 'r', 'u.FK_rola = r.ID_rola')
                ->where('u.ID_pracownik = :id')
                ->setParameter(':id', $userId, \PDO::PARAM_INT);
            $result = $queryBuilder->execute()->fetchAll();

            if ($result) {
                    $roles = array_column($result, 'nazwa');
            }

            return $roles;
        } catch (DBALException $exception) {
            return $roles;
        }
    }

    /**
     * Pobierz id firmy i id parkingu
     *
     * @param string $login
     *
     * @return mixed
     */
    public function pobierzIDFirmyIParkingu($login)
    {
        $queryBuilder = $this->pobierz();

        $queryBuilder->where('p.login = :login')
            ->setParameter(':login', $login, \PDO::PARAM_STR);

        $result = $queryBuilder->execute()->fetch();

        return $result;
    }


    /**
     * Find for uniqueness.
     *
     * @param string          $login Element name
     * @param int|string|null $id    Element id
     *
     * @return array Result
     */
    public function sprawdzCzyUnikalne($login, $id = null)
    {
        $queryBuilder = $this->pobierz();
        $queryBuilder->where('p.login = :login')
            ->setParameter(':login', $login, \PDO::PARAM_STR);
        if ($id) {
            $queryBuilder->andWhere('p.ID_pracownik <> :id')
                ->setParameter(':id', $id, \PDO::PARAM_INT);
        }

        return $queryBuilder->execute()->fetchAll();
    }


    /**
     * Tworzy queryBuilder do pobrania danych (z ewentualnym idFirma).
     *
     * @param int $idFirma
     *
     * @return \Doctrine\DBAL\Query\QueryBuilder
     */
    private function pobierz($idFirma = -1)
    {
        $queryBuilder = $this->db->createQueryBuilder();

        $queryBuilder->select('p.ID_pracownik', 'p.login', 'd.imie', 'd.nazwisko', 'd.email', 'f.nazwa as firma_nazwa', 'pa.nazwa as parking_nazwa', 'r.nazwa as rola_nazwa', 'p.FK_firma', 'p.FK_parking', 'p.FK_rola')
            ->from('Pracownik', 'p')
            ->leftJoin('p', 'Dane_pracownika', 'd', 'p.ID_pracownik = d.FK_pracownik')
            ->leftJoin('p', 'Firma', 'f', 'p.FK_firma = f.ID_firma')
            ->leftJoin('p', 'Rola', 'r', 'p.FK_rola = r.ID_rola')
            ->leftJoin('p', 'Parking', 'pa', 'pa.ID_parking = p.FK_parking');

        if (-1 !== $idFirma) {
            $queryBuilder->where('p.FK_firma = :idFirma')
                ->setParameter(':idFirma', $idFirma, \PDO::PARAM_INT);
        }

        return $queryBuilder;
    }
}
