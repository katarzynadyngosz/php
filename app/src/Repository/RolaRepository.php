<?php
/**
 * Rola repository.
 */
namespace Repository;

use Doctrine\DBAL\Connection;

/**
 * Class RolaRepository.
 */
class RolaRepository
{
    /**
     * Doctrine DBAL connection.
     *
     * @var \Doctrine\DBAL\Connection $db
     */
    protected $db;

    /**
     * RolaRepository constructor.
     *
     * @param \Doctrine\DBAL\Connection $db
     */
    public function __construct(Connection $db)
    {
        $this->db = $db;
    }

    /**
     * Pobierz role
     *
     * @return array
     */
    public function pobierz()
    {
        $queryBuilder = $this->db->createQueryBuilder();

        $queryBuilder->select('ID_rola', 'nazwa')
            ->from('Rola');

        $result = $queryBuilder->execute()->fetchAll();

        return $result;
    }

    /**
     * Pobierz id admina
     *
     * @return mixed
     */
    public function pobierzRolaAdmin()
    {
        $queryBuilder = $this->db->createQueryBuilder();

        $queryBuilder->select('ID_rola', 'nazwa')
            ->from('Rola')
            ->where('nazwa = :pracownik')
            ->setParameter(':pracownik', "ROLE_ADMIN", \PDO::PARAM_STR);

        $result = $queryBuilder->execute()->fetch();

        return $result['ID_rola'];
    }

    /**
     * Pobierz id firma
     *
     * @return mixed
     */
    public function pobierzRolaFirma()
    {
        $queryBuilder = $this->db->createQueryBuilder();

        $queryBuilder->select('ID_rola', 'nazwa')
            ->from('Rola')
            ->where('nazwa = :pracownik')
            ->setParameter(':pracownik', "ROLE_FIRMA", \PDO::PARAM_STR);

        $result = $queryBuilder->execute()->fetch();

        return $result['ID_rola'];
    }
}
