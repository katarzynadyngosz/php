<?php
/**
 * Firma repository.
 */
namespace Repository;

use Doctrine\DBAL\Connection;
use Utils\Paginator;

/**
 * Class FirmaRepository.
 */
class FirmaRepository
{
    /**
     * Doctrine DBAL connection.
     *
     * @var \Doctrine\DBAL\Connection $db
     */
    protected $db;

    /**
     * Number of items per page.
     *
     * const int NUM_ITEMS
     */
    const NUM_ITEMS = 5;

    /**
     * FirmaRepository constructor.
     *
     * @param \Doctrine\DBAL\Connection $db
     */
    public function __construct(Connection $db)
    {
        $this->db = $db;
    }



    /**
     * Zwróć wszystkie dane
     *
     * @return array
     */
    public function pobierzWszystko()
    {
        $queryBuilder = $this->pobierz();

        $result = $queryBuilder->execute()->fetchAll();

        return $result;
    }

    /**
     * Zwróć wszystkie dane zpaginowane.
     *
     * @param int $page
     *
     * @return array
     */
    public function pobierzPaginated($page)
    {
        $countQueryBuilder = $this->pobierz()
            ->select('COUNT(ID_firma) AS total_results')
            ->setMaxResults(1);

        $paginator = new Paginator($this->pobierz(), $countQueryBuilder);
        $paginator->setCurrentPage($page);
        $paginator->setMaxPerPage(self::NUM_ITEMS);

        return $paginator->getCurrentPageResults();
    }

    /**
     * Zwróć dane firmy o danym id.
     *
     * @param int $id
     *
     * @return mixed
     */
    public function pobierzDaneJednego($id)
    {
        $queryBuilder = $this->pobierz();

        $queryBuilder->where('ID_firma = :id')
            ->setParameter(':id', $id, \PDO::PARAM_INT);

        $result = $queryBuilder->execute()->fetch();

        return $result;
    }

    /**
     * Edytuj firmę o danym id
     *
     * @param array $dane
     * @param int   $id
     */
    public function edytuj($dane, $id)
    {
        unset($dane['dane']);
        $this->db->update('Firma', $dane, ['ID_firma' => $id]);
    }

    /**
     * Dodaj firmę
     *
     * @param array $dane
     */
    public function dodaj($dane)
    {
        unset($dane['dane']);
        $this->db->insert('Firma', $dane);
    }

    /**
     * Usuń firmę o danym id
     *
     * @param int $id
     *
     * @return int
     *
     * @throws \Doctrine\DBAL\Exception\InvalidArgumentException
     */
    public function usun($id)
    {
        return $this->db->delete('Firma', ['ID_firma' => $id]);
    }

    /**
     * Sprawdź, czy istnieje firma o takiej nazwie
     *
     * @param string $nazwa
     * @param int    $id
     *
     * @return array
     */
    public function sprawdzNazwe($nazwa, $id)
    {
        $queryBuilder = $this->pobierz();
        $queryBuilder->where('nazwa = :nazwa')
            ->setParameter(':nazwa', $nazwa, \PDO::PARAM_STR);
        if ($id) {
            $queryBuilder->andWhere('ID_firma <> :id')
                ->setParameter(':id', $id, \PDO::PARAM_INT);
        }

        return $queryBuilder->execute()->fetchAll();
    }

    /**
     * Sprawdź, czy istnieje firma o takim mailu.
     *
     * @param string $email
     * @param int    $id
     *
     * @return array
     */
    public function sprawdzEmail($email, $id)
    {
        $queryBuilder = $this->pobierz();
        $queryBuilder->where('email = :email')
            ->setParameter(':email', $email, \PDO::PARAM_STR);
        if ($id) {
            $queryBuilder->andWhere('ID_firma <> :id')
                ->setParameter(':id', $id, \PDO::PARAM_INT);
        }

        return $queryBuilder->execute()->fetchAll();
    }

    /**
     * Zwróć queryBuilder, który pobiera wszystkie dane.
     *
     * @return \Doctrine\DBAL\Query\QueryBuilder
     */
    private function pobierz()
    {
        $queryBuilder = $this->db->createQueryBuilder();

        $queryBuilder->select('ID_firma', 'nazwa', 'email', 'nip', 'miasto')
            ->from('Firma');

        return $queryBuilder;
    }
}
