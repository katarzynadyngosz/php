<?php
/**
 * Wjazd repository.
 */
namespace Repository;

use Doctrine\DBAL\Connection;
use Utils\Paginator;

/**
 * Class WjazdRepository.
 */
class WjazdRepository
{
    /**
     * Doctrine DBAL connection.
     *
     * @var \Doctrine\DBAL\Connection $db
     */
    protected $db;

    /**
     * Number of items per page.
     *
     * const int NUM_ITEMS
     */
    const NUM_ITEMS = 5;

    /**
     * WjazdRepository constructor.
     *
     * @param \Doctrine\DBAL\Connection $db
     */
    public function __construct(Connection $db)
    {
        $this->db = $db;
    }

    /**
     * Dodaj wjazd z danym numerem biletu do danego parkingu
     *
     * @param string $nrBiletu
     * @param int    $idParking
     */
    public function dodaj($nrBiletu, $idParking)
    {
        $godzina = date("Y-m-d H:i:s");
        $this->db->insert('Wjazd', ['czas_wjazdu' => $godzina, 'FK_parking' => $idParking, 'nr_biletu' => $nrBiletu]);
    }

    /**
     * Zweryfikuj, czy istnieje dany bilet przypisany do danego parkingu
     *
     * @param string $nrBiletu
     * @param int    $idParkingu
     *
     * @return mixed
     */
    public function zweryfikuj($nrBiletu, $idParkingu)
    {
        $queryBuilder = $this->db->createQueryBuilder();

        $queryBuilder->select('ID_wjazd', 'nr_biletu', 'czas_wyjazdu', 'czas_wjazdu', 'FK_parking')
            ->from('Wjazd')
            ->where('nr_biletu = :nr_biletu')
            ->setParameter(':nr_biletu', $nrBiletu, \PDO::PARAM_STR)
            ->andWhere('FK_parking = :idParkingu')
            ->setParameter(':idParkingu', $idParkingu, \PDO::PARAM_INT);

        $result = $queryBuilder->execute()->fetch();

        return $result;
    }

    /**
     * Oblicz należną kwotę za postój.
     *
     * @param double $cenaZaGodzine
     * @param array  $dane
     *
     * @return float|int
     */
    public function obliczKwote($cenaZaGodzine, $dane)
    {
        $czasWjazdu = new \DateTime($dane['czas_wjazdu']);
        $czasWyjazdu = new \DateTime();

        $dane['czas_wyjazdu'] = date("Y-m-d H:i:s");
        $dane['koszt'] = $cenaZaGodzine * $czasWyjazdu->diff($czasWjazdu)->h +
            24 * $cenaZaGodzine * $czasWyjazdu->diff($czasWjazdu)->d;

        $this->db->update('Wjazd', $dane, ['ID_wjazd' => $dane['ID_wjazd']]);

        return $dane['koszt'];
    }

    /**
     * Pobierz dane zpaginowane
     *
     * @param int $page
     * @param int $idFirma
     * @param int $idParking
     *
     * @return array
     */
    public function pobierzPaginated($page, $idFirma, $idParking)
    {
        $countQueryBuilder = $this->pobierz($idFirma, $idParking)
            ->select('COUNT(ID_wjazd) AS total_results')
            ->setMaxResults(1);

        $paginator = new Paginator($this->pobierz($idFirma, $idParking), $countQueryBuilder);
        $paginator->setCurrentPage($page);
        $paginator->setMaxPerPage(self::NUM_ITEMS);

        return $paginator->getCurrentPageResults();
    }

    /**
     * Pobierz dane jednego wjazdu
     *
     * @param int $id
     *
     * @return mixed
     */
    public function pobierzDaneJednego($id)
    {
        $queryBuilder = $this->pobierz();

        $queryBuilder->where('ID_wjazd = :id')
            ->setParameter(':id', $id, \PDO::PARAM_INT);

        $result = $queryBuilder->execute()->fetch();

        return $result;
    }

    /**
     * Edytuj dane wjazdu
     *
     * @param array $dane
     * @param int   $id
     */
    public function edytuj($dane, $id)
    {
        unset($dane['dane']);
        if (isset($dane['parking'])) {
            $dane['FK_parking'] = $dane['parking'];
        }

        unset($dane['parking']);
        $dane['czas_wjazdu'] = $dane['czas_wjazdu']->format("Y-m-d H:i:s");

        if ($dane['czy_czas_wyjazdu']) {
            $dane['czas_wyjazdu'] = $dane['czas_wyjazdu']->format("Y-m-d H:i:s");
        } else {
            unset($dane['czas_wyjazdu']);
            unset($dane['koszt']);
        }

        unset($dane['czy_czas_wyjazdu']);

        $this->db->update('Wjazd', $dane, ['ID_wjazd' => $id]);
    }

    /**
     * Usuń wjazd o danym id
     *
     * @param int $id
     *
     * @return int
     *
     * @throws \Doctrine\DBAL\Exception\InvalidArgumentException
     */
    public function usun($id)
    {
        return $this->db->delete('Wjazd', ['ID_wjazd' => $id]);
    }

    /**
     * Pobierz wjazdy przypisane do danej firmy/parkingu
     *
     * @param int $idFirma
     * @param int $idParking
     *
     * @return \Doctrine\DBAL\Query\QueryBuilder
     */
    private function pobierz($idFirma = -1, $idParking = -1)
    {
        $queryBuilder = $this->db->createQueryBuilder();

        $queryBuilder->select('ID_wjazd', 'nr_biletu', 'p.nazwa as parking', 'koszt', 'czas_wjazdu', 'czas_wyjazdu', 'p.FK_firma', 'p.ID_parking', 'FK_parking')
            ->from('Wjazd', 'w')
            ->join('w', 'Parking', 'p', 'p.ID_parking = w.FK_parking');

        if (-1 !== $idFirma) {
            $queryBuilder->where('p.FK_firma = :idFirma')
                ->setParameter(':idFirma', $idFirma, \PDO::PARAM_INT);
            if (-1 !== $idParking) {
                $queryBuilder->andWhere('p.ID_parking = :idParking')
                    ->setParameter(':idParking', $idParking, \PDO::PARAM_INT);
            }
        }


        return $queryBuilder;
    }
}
