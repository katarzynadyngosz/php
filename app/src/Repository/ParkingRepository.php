<?php
/**
 * Parking repository.
 */
namespace Repository;

use Doctrine\DBAL\Connection;
use Utils\Paginator;

/**
 * Class ParkingRepository.
 */
class ParkingRepository
{
    /**
     * Doctrine DBAL connection.
     *
     * @var \Doctrine\DBAL\Connection $db
     */
    protected $db;

    /**
     * Number of items per page.
     *
     * const int NUM_ITEMS
     */
    const NUM_ITEMS = 5;

    /**
     * ParkingRepository constructor.
     *
     * @param \Doctrine\DBAL\Connection $db
     */
    public function __construct(Connection $db)
    {
        $this->db = $db;
    }

    /**
     * Pobierz wszystkie dane.
     *
     * @return array
     */
    public function pobierzWszystko()
    {
        $queryBuilder = $this->pobierz();

        $result = $queryBuilder->execute()->fetchAll();

        return $result;
    }

    /**
     * Pobierz wszystkie dane zpaginowane z ewentualnym id Parkingu
     *
     * @param int $page
     * @param int $idParking
     *
     * @return array
     */
    public function pobierzPaginated($page, $idParking)
    {
        $countQueryBuilder = $this->pobierz($idParking)
            ->select('COUNT(p.ID_parking) AS total_results')
            ->setMaxResults(1);

        $paginator = new Paginator($this->pobierz($idParking), $countQueryBuilder);
        $paginator->setCurrentPage($page);
        $paginator->setMaxPerPage(self::NUM_ITEMS);

        return $paginator->getCurrentPageResults();
    }

    /**
     * Dodaj parking
     *
     * @param array $dane
     */
    public function dodaj($dane)
    {
        unset($dane['dane']);
        $dane['FK_firma'] = $dane['firma'];
        unset($dane['firma']);
        $this->db->insert('Parking', $dane);
    }

    /**
     * Pobierz dane jednego parkingu
     *
     * @param int $id
     *
     * @return mixed
     */
    public function pobierzDaneJednego($id)
    {
        $queryBuilder = $this->pobierz();

        $queryBuilder->where('p.ID_parking = :id')
            ->setParameter(':id', $id, \PDO::PARAM_INT);

        $result = $queryBuilder->execute()->fetch();

        return $result;
    }

    /**
     * Edytuj dane parkingu o danym id
     *
     * @param array $daneZFormularza
     * @param int   $id
     */
    public function edytuj($daneZFormularza, $id)
    {
        unset($daneZFormularza['dane']);
        $daneZFormularza['FK_firma'] = $daneZFormularza['firma'];
        unset($daneZFormularza['firma']);
        $this->db->update('Parking', $daneZFormularza, ['ID_parking' => $id]);
    }

    /**
     * Usuń parking o danym id
     *
     * @param int $id
     *
     * @return int
     *
     * @throws \Doctrine\DBAL\Exception\InvalidArgumentException
     */
    public function usun($id)
    {
        return $this->db->delete('Parking', ['ID_parking' => $id]);
    }

    /**
     * Pobierz cene parkingu o danym id
     *
     * @param int $id
     *
     * @return mixed
     */
    public function pobierzCene($id)
    {
        $queryBuilder = $this->pobierz();

        $queryBuilder->where('p.ID_parking = :id')
            ->setParameter(':id', $id, \PDO::PARAM_INT);

        $result = $queryBuilder->execute()->fetch();

        return $result;
    }

    /**
     * Find for uniqueness.
     *
     * @param string          $nazwa Element name
     * @param int|string|null $id    Element id
     *
     * @return array Result
     */
    public function sprawdzNazwe($nazwa, $id = null)
    {
        $queryBuilder = $this->pobierz();
        $queryBuilder->where('p.nazwa = :nazwa')
            ->setParameter(':nazwa', $nazwa, \PDO::PARAM_STR);
        if ($id) {
            $queryBuilder->andWhere('p.ID_parking <> :id')
                ->setParameter(':id', $id, \PDO::PARAM_INT);
        }

        return $queryBuilder->execute()->fetchAll();
    }

    /**
     * Pobierz dane parkingów firmy o danym id
     *
     * @param int $idFirmy
     *
     * @return \Doctrine\DBAL\Query\QueryBuilder
     */
    private function pobierz($idFirmy = -1)
    {
        $queryBuilder = $this->db->createQueryBuilder();

        $queryBuilder->select('p.ID_parking', 'p.nazwa as parking_nazwa', 'p.liczba_miejsc', 'p.kwota_godzina', 'f.nazwa as firma_nazwa', 'p.FK_firma')
            ->from('Parking', 'p')
            ->join('p', 'Firma', 'f', 'f.ID_firma = p.FK_firma');

        if (-1 !== $idFirmy) {
            $queryBuilder->where('p.FK_firma = :idFirmy')
                ->setParameter(':idFirmy', $idFirmy, \PDO::PARAM_INT);
        }

        return $queryBuilder;
    }
}
