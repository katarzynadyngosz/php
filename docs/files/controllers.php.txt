<?php
/**
 * Controller file
 */
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Controller\WjazdController;
use Controller\AdminWjazdController;
use Controller\AdminPracownikController;
use Controller\ParkingController;
use Controller\KodController;
use Controller\WyjazdController;
use Controller\AuthController;
use Controller\AdminController;
use Controller\AdminFirmaController;
use Controller\AdminParkingController;
use Controller\AdminZalogowanyController;

//Request::setTrustedProxies(array('127.0.0.1'));

$app->mount('/admin', new AdminController());
$app->mount('/admin/firma', new AdminFirmaController());
$app->mount('/admin/parking', new AdminParkingController());
$app->mount('/auth', new AuthController());
$app->mount('/kod', new KodController());
$app->mount('/', new ParkingController());
$app->mount('/wyjazd', new WyjazdController());
$app->mount('/wjazd', new WjazdController());
$app->mount('/admin/wjazd', new AdminWjazdController());
$app->mount('/admin/pracownik', new AdminPracownikController());
$app->mount('/admin/zalogowany', new AdminZalogowanyController());

$app->error(function (\Exception $e, Request $request, $code) use ($app) {
    if ($app['debug']) {
        return;
    }

    // 404.html, or 40x.html, or 4xx.html, or error.html
    $templates = array(
        'errors/'.$code.'.html.twig',
        'errors/'.substr($code, 0, 2).'x.html.twig',
        'errors/'.substr($code, 0, 1).'xx.html.twig',
        'errors/default.html.twig',
    );

    return new Response($app['twig']->resolveTemplate($templates)->render(array('code' => $code)), $code);
});

