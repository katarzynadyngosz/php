<?php
/**
 * Wyjazd controller
 */
namespace Controller;

use Repository\ParkingRepository;
use Repository\WjazdRepository;
use Silex\Application;
use Silex\Api\ControllerProviderInterface;
use Form\WyjazdType;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class WyjazdController
 */
class WyjazdController implements ControllerProviderInterface
{
    /**
     * Routing settings.
     *
     * @param \Silex\Application $app Silex application
     *
     * @return \Silex\ControllerCollection Result
     */
    public function connect(Application $app)
    {
        $controller = $app['controllers_factory'];
        $controller->match('/{id}', [$this, 'indexAction'])
            ->method('POST|GET')
            ->assert('id', '[1-9][0-9]*')
            ->bind('wyjazd');

        return $controller;
    }

    /**
     * Index action.
     *
     * @param \Silex\Application                        $app
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param int                                       $id
     *
     * @return mixed
     */
    public function indexAction(Application $app, Request $request, $id)
    {
        $tag = [];

        $form = $app['form.factory']->createBuilder(WyjazdType::class, $tag)->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $dane = $form->getData();

            $wjazdRepository = new WjazdRepository($app['db']);
            $wynik = $wjazdRepository->zweryfikuj($dane['nr_biletu'], $id);
            if ($wynik) {
                if ($wynik['czas_wyjazdu']) {
                    $app['session']->getFlashBag()->add(
                        'messages',
                        [
                            'type' => 'danger',
                            'message' => 'komunikat.wykorzystanywyjazd',
                        ]
                    );
                } else {
                    $parkingRepository = new ParkingRepository($app['db']);
                    $cenaZaGodzine = $parkingRepository->pobierzCene($id);

                    $koszt = $wjazdRepository->obliczKwote($cenaZaGodzine['kwota_godzina'], $wynik);

                    return $app['twig']->render(
                        'Wyjazd/zaplac.html.twig',
                        ['koszt' => $koszt,
                        'id' => $id, ]
                    );
                }
            } else {
                $app['session']->getFlashBag()->add(
                    'messages',
                    [
                        'type' => 'danger',
                        'message' => 'komunikat.blednykod',
                    ]
                );
            }
        }

        return $app['twig']->render(
            'Wyjazd/wyjazd.html.twig',
            ['id' => $id,
            'form' => $form->createView(), ]
        );
    }
}

