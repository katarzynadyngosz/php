<?php
/**
 * Wjazd controller
 */
namespace Controller;

use Silex\Application;
use Silex\Api\ControllerProviderInterface;

/**
 * Class WjazdController
 */
class WjazdController implements ControllerProviderInterface
{
    /**
     * Routing settings.
     *
     * @param \Silex\Application $app Silex application
     *
     * @return \Silex\ControllerCollection Result
     */
    public function connect(Application $app)
    {
        $controller = $app['controllers_factory'];
        $controller->get('/{id}', [$this, 'indexAction'])
            ->assert('id', '[1-9][0-9]*')
            ->bind('wjazd');

        return $controller;
    }

    /**
     * Index action.
     *
     * @param \Silex\Application $app
     * @param int                $id
     *
     * @return mixed
     */
    public function indexAction(Application $app, $id)
    {

        return $app['twig']->render(
            'Wjazd/wjazd.html.twig',
            ['id' => $id]
        );
    }
}

